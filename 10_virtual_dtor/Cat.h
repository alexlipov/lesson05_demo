#pragma once
#include "Animal.h"
#include <string>

#define COLOR_BLACK		0x000000
#define COLOR_WHITE		0xFFFFFF
#define COLOR_RED		0xFF0000
#define COLOR_YELLOW	0xFFFF00

class Cat : public Animal
{
	//int* _colors;
	std::string somethingCatHas; // how is string implemented?
public:
	Cat(std::string name);
	~Cat();

	void saySomething();
};

