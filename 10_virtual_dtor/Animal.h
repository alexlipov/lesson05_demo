#pragma once
#include <string>

class Animal
{
protected:
	std::string _name;

public:
	Animal(std::string name);
	/*virtual*/ ~Animal();

	std::string getName();
	virtual void saySomething();
	virtual void isHumanBestFriend();
};

