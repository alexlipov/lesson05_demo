#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

Cat* createCat() {
	//int* colors = new int[2];
	//colors[0] = COLOR_WHITE;
	//colors[1] = COLOR_BLACK;
	//Cat* garfield = new Cat("Garfield", colors);

	Cat* garfield = new Cat("Garfield");
	return garfield;
}

class Bla {
public:
	Bla() {

	}
	~Bla() {

	}
};

class Hey {
public:
	Bla bla;
	Hey() {

	}
	~Hey() {

	}
};

#include <cmath>

void main() {
	Animal* garfield = createCat(); // upcasting
	garfield->saySomething();
	garfield->isHumanBestFriend();
	delete garfield; // static-binding vs dynamic-binding

	// the program keeps running..




	// rule of thumb:
	// A base class constructor should be either: 
	// - public and virtual 
	//  or 
	// - protected and non-virtual







	// TODO: add example for compiler-generated
	// destructor call for class-value fields
	// i.e. a field of type Y in class X
	Hey hey;

	

}