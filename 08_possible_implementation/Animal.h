#pragma once
#include <string>

typedef void(*func_saySomething)(Animal*);

class Animal
{
protected:
	std::string _name;
	func_saySomething* _func_saySomething;

	Animal(std::string name, func_saySomething* ptr);
public:
	Animal(std::string name);

	std::string getName();
	void saySomething();
};

