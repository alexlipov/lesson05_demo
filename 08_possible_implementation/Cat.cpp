#include "Cat.h"
#include <iostream>

// note: it's about the same what compiler does, but 
// unfortunately we can't mimic it exactly

Cat::Cat(std::string name)
	: Animal(name, &Cat::saySomething) {
}

void Cat::saySomething() {
	std::cout << "Cat " << _name <<
		" says meow!" << std::endl;
}