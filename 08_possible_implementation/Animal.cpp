#include "Animal.h"
#include <iostream>

Animal::Animal(std::string name) {
	this->_name = name;
	this->_func_saySomething = nullptr;
}

Animal::Animal(std::string name, func_saySomething* ptr) {
	this->_name = name;
	this->_func_saySomething = ptr;
}

std::string Animal::getName() {
	return _name;
}

void Animal::saySomething() {
	// trying to implement dynamic binding
	if (_func_saySomething != nullptr) {
		(*_func_saySomething)(this);
	}
	else {
		// original code
		std::cout << "Animal " << _name <<
			" says something" << std::endl;
	}
}