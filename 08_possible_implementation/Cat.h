﻿#pragma once
#include "Animal.h"
#include <string>

class Cat : public Animal
{
	static func_saySomething catFunc;
public:
	Cat(std::string name);

	void saySomething();
};

