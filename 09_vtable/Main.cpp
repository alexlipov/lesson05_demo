#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void main() {
	Cat garfield("Garfield");
	// garfield.saySomething()
	Dog scoobyDoo("Scooby Doo");
	// scoobyDoo.saySomething()

	Animal* animals[2];
	animals[0] = &garfield;
	animals[1] = &scoobyDoo;

	for (int i = 0; i < 2; i++) {
		animals[i]->saySomething();
		animals[i]->isHumanBestFriend();
	}
	// show disassembly
}