#pragma once
#include <string>

class Animal
{
protected:
	std::string _name;

public:
	Animal(std::string name);

	std::string getName();
	
	virtual void saySomething();
	virtual void saySomething(std::string personName);

	virtual void isHumanBestFriend();
};

