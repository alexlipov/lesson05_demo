#include <typeinfo>
#include <iostream>
#include "Animal.h"
#include "Cat.h"
#include "Dog.h"


Animal* dynamic_cast_implementation(Animal* base, const std::type_info& targetType) {
	// Get the actual runtime type of the object
	const std::type_info& actualType = typeid(*base);

	// Compare runtime type with target type
	if (actualType == targetType) {
		return base; // The cast is valid
	}

	return nullptr; // The cast is invalid
}


void main() {
	Cat garfield("Garfield");

	// upcasting: class inheritance allows to point on derived class's objects
	// STATIC CAST is used here (perfomed at compile time)
	Animal* animalPointer = &garfield;
	animalPointer->saySomething();
	// animalPointer->isPurring();

	
	// downcasting
	Animal unicorn("Unicorn");
	// Cat* catPointer2 = &unicorn;

	// or with classes we don't inherit
	Dog scoobyDoo("Scooby Doo");
	// Cat* catPointer3 = &scoobyDoo;

	// C-style static cast: 
	// - works, but this is dangerous
	// - will succeed regardless of whether the object is of the derived type Cat or not
	// - leads to undefined behavior if the object is not actually of type Cat
	// - therefore the program can crash at runtime
	Cat* catPointerC = (Cat*)animalPointer;
	Dog* dogPointerC = (Dog*)animalPointer;
	dogPointerC->isHumanBestFriend();
	Cat* unicornPointerC = (Cat*)&unicorn;
	unicornPointerC->isPurring();
	Dog* dogPointerC2 = (Dog*)&garfield; // cast succeeds!


	// C++'s static cast:
	// - enforces type-checking at compile time
	// - does not perform runtime checks to ensure the pointer actually points to a Cat object
	Cat* catPointerCpp = static_cast<Cat*>(animalPointer);
	// Dog* catPointerCpp = static_cast<Dog*>(&garfield); // cast fails!



	// C++'s dynamic cast:
	// safe downcasting
	// - performs runtime checks to ensure the pointer actually points to a Cat object
	Cat* dynamicCat = dynamic_cast<Cat*>(animalPointer);
	Dog* dynamicDog = dynamic_cast<Dog*>(animalPointer);
	Cat* dynamicUnicorn = dynamic_cast<Cat*>(&unicorn);


	

	// typeid
	const std::type_info& actualType = typeid(Cat);
	std::cout << actualType.name();

	// possible implementation:
	Cat* catPointerDyn = (Cat*)dynamic_cast_implementation(animalPointer, typeid(Cat));
	Cat* catPointerDyn2 = (Cat*)dynamic_cast_implementation(&unicorn, typeid(Cat));
}