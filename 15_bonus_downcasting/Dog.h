#pragma once
#include "Animal.h"
class Dog : public Animal
{
public:
	Dog(std::string name);

	void saySomething() override;
	void saySomething(std::string personName) override;

	void isHumanBestFriend() override;
};

