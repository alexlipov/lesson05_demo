#include "Cat.h"
#include <iostream>

Cat::Cat(std::string name)
	: Animal() {
	this->_name = name;
}

void Cat::saySomething() {
	std::cout << "Cat " << _name <<
		" says meow!" << std::endl;
}

void Cat::isHumanBestFriend() {
	std::cout << "Cat " << _name <<
		" is not a human's best friend" << std::endl;
}