#pragma once
#include <string>

class Animal
{
public:
	virtual void saySomething() = 0;
	virtual void isHumanBestFriend() = 0;
};

