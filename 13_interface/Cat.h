﻿#pragma once
#include "Animal.h"
#include <string>

class Cat : public Animal
{
protected:
	std::string _name;
	bool _purring; // מגרגר
public:
	Cat(std::string name);

	void saySomething() override;
	void isHumanBestFriend() override;

	void setPurring(bool value) /*override*/;
	bool isPurring() /*override*/;
};

