#pragma once
#include "Animal.h"
class Dog : public Animal
{
protected:
	std::string _name;
public:
	Dog(std::string name);

	void saySomething() override;
	void isHumanBestFriend() override;
};

