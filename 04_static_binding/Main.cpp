#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

// static binding; 
// method that will be called is determined 
// by variable's type
void main() {
	Cat garfield("Garfield");

	Cat* catPointer = &garfield;
	catPointer->saySomething();

	Animal* animalPointer = &garfield;
	animalPointer->saySomething();
}