#include <iostream>

long add(int left, int right)
{
	return left + right;
}

long mul(int left, int right)
{
	return left * right;
}

// OP is a function pointer
typedef long(*OP)(int, int);

void main()
{
	OP funcptr[2];
	funcptr[0] = add;
	funcptr[1] = mul;

	for (int i = 0; i < 2; i++)
		std::cout << "result:" << funcptr[i](5, 7) << std::endl;
}
