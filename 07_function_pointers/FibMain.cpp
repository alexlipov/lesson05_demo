#include <iostream>

unsigned long func(unsigned int value) 
{
    if (value < 2)
        return value;
    return func(value - 1) + func(value - 2);
}

void main2() 
{
    unsigned long result = func(7);
    std::cout << "result: " << result << std::endl;
}


void main3()
{
    unsigned long (*func_ptr)(unsigned int) = func;
    unsigned long result = func_ptr(7);
    std::cout << "result: " << result << std::endl;
}

