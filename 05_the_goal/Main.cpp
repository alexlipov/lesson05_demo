#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void attempt1() {
	Cat garfield("Garfield");
	Dog scoobyDoo("Scooby Doo");

	Animal animals[2];

	animals[0] = garfield;
	animals[1] = scoobyDoo;

	for (int i = 0; i < 2; i++) {
		animals[i].saySomething();
	}
}

// static binding; 
// method called is determined by variable's type
void attempt2() {
	Cat garfield("Garfield");
	Dog scoobyDoo("Scooby Doo");

	Animal* animals[2];
	animals[0] = &garfield;
	animals[1] = &scoobyDoo;

	for (int i = 0; i < 2; i++) {
		animals[i]->saySomething();
	}

	// we want to cut the tie between
	// variable type and invoked method;
	// however we lose some information
}

void main() {
	Cat garfield("Garfield");
	// garfield.saySomething();

	Dog scoobyDoo("Scooby Doo");
	// scoobyDoo.saySomething();

	// the goal:
	//
	// Animal animals[2];
	// animals[0] = garfield;
	// animals[1] = scoobyDoo;
	// for (int i = 0; i < 2; i++) {
	//     animals[i].saySomething();
	// }
	//
	// will print:
	// Cat Garfield says meow!
	// Dog Scooby Doo says woof!

	attempt1();
	attempt2();
}