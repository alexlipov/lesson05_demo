#include "Cat.h"
#include <iostream>

Cat::Cat(std::string name)
	: Animal(name) {

}

void Cat::saySomething() {
	std::cout << "Cat " << _name <<
		" says meow!" << std::endl;
}