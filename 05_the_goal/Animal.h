#pragma once
#include <string>

class Animal
{
protected:
	std::string _name;

public:
	Animal() {};
	Animal(std::string name);

	std::string getName();
	void saySomething();
};

