#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void main() {
	Cat garfield("Garfield");
	garfield.saySomething();

	Dog scoobyDoo("Scooby Doo");
	scoobyDoo.saySomething();

	// show debug view (how VS shows split class hierarchy state)
}