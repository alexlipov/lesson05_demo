#include "Animal.h"
#include <iostream>

Animal::Animal(std::string name) {
	this->_name = name;
}

std::string Animal::getName() {
	return _name;
}

void Animal::saySomething() {
	std::cout << "Animal " << _name <<
		" says something" << std::endl;
}