#include "Dog.h"
#include <iostream>

Dog::Dog(std::string name)
	: Animal(name) {

}

void Dog::saySomething() {
	std::cout << "Dog " << _name <<
		" says woof!" << std::endl;
}

void Dog::saySomething(std::string personName) {
	std::cout << "Dog " << _name <<
		" says something to " <<
		personName << std::endl;
}

void Dog::isHumanBestFriend() {
	std::cout << "Dog " << _name <<
		" is human's best friend" << std::endl;
}