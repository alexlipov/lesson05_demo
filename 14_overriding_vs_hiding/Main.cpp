#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void main() {
	Cat garfield("Garfield");
	// method hiding still takes effect, even though the method
	// is defined is virtual.
	// this is not possible:
	// garfield.saySomething("Steven");
	Dog scoobyDoo("Scooby Doo");
	scoobyDoo.saySomething("Steven");

	Animal* animals[2];
	animals[0] = &garfield;
	animals[1] = &scoobyDoo;

	for (int i = 0; i < 2; i++) {
		animals[i]->saySomething();
		animals[i]->saySomething("Steven");
		animals[i]->isHumanBestFriend();
	}
}