﻿#pragma once
#include "Animal.h"
#include <string>

class Cat : public Animal
{
	bool _purring; // מגרגר
public:
	Cat(std::string name);

	void saySomething() override;

	void setPurring(bool value) /*override*/;
	bool isPurring() /*override*/;
};

