#pragma once
#include "Animal.h"
class Dog : public Animal
{
public:
	Dog(std::string name);
	Dog(const Dog& other);

	void saySomething() override;
	void isHumanBestFriend() override;
};

