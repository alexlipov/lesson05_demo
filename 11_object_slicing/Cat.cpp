#include "Cat.h"
#include <iostream>

Cat::Cat(std::string name)
	: Animal(name) {

}

Cat::Cat(const Cat& other) 
	: Animal(other) {

}

void Cat::saySomething() {
	std::cout << "Cat " << _name <<
		" says meow!" << std::endl;
}

void Cat::setPurring(bool value) {
	_purring = value;
}
bool Cat::isPurring() {
	return _purring;
}