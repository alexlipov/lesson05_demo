#include "Animal.h"
#include "Cat.h"
#include "Dog.h"
#include <iostream>

void slice(Animal animal) {
	animal.saySomething();
}

void doNotSlice(Animal& animal) {
	animal.saySomething();
}

void main() {
	Cat garfield("Garfield");

	std::cout << "do not slice: " << std::endl;
	doNotSlice(garfield); // upcasting
	std::cout << "slice: " << std::endl;
	slice(garfield); // why?
}
