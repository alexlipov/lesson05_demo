#include "Animal.h"
#include <iostream>

Animal::Animal(std::string name) {
	this->_name = name;
}

Animal::Animal(const Animal& other) {
	this->_name = other._name;
}

std::string Animal::getName() {
	return _name;
}

void Animal::saySomething() {
	std::cout << "Animal " << _name <<
		" says something" << std::endl;
}

void Animal::isHumanBestFriend() {
	std::cout << "Animal " << _name <<
		" is not a human's best friend" << std::endl;
}