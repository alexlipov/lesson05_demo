#pragma once
#include <string>

class Animal
{
protected:
	std::string _name;

public:
	Animal(std::string name);

	std::string getName();
	void saySomething();
	void saySomething(std::string personName);
};

