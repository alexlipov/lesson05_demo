#include "Cat.h"
#include <iostream>

Cat::Cat(std::string name)
	: Animal(name) {

}

void Cat::saySomething(int i) {
	std::cout << "Cat " << _name <<
		" says meow" << i << "times!" << std::endl;
}