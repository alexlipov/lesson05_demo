#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void main() {
	Animal unicorn("Unicorn");
	unicorn.saySomething();
	unicorn.saySomething("Steven");

	Cat garfield("Garfield");
	garfield.saySomething(5);
	// garfield.saySomething("Steven");

	Dog scoobyDoo("Scooby Doo");
	scoobyDoo.saySomething();
	// scoobyDoo.saySomething("Steven");

	// but what if we really want to call it?
	// ------------------------------------------------------>>>>>  
	scoobyDoo.Animal::saySomething("Steven");
}