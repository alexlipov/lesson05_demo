#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

void main() {
	Cat garfield("Garfield");

	Cat* catPointer = &garfield;
	catPointer->saySomething();
	catPointer->isPurring();





	// class inheritance allows to point on 
	// derived class's objects 
	Animal* animalPointer = &garfield;
	animalPointer->saySomething();
	// animalPointer->isPurring();
	
	// void* allows to point on anything
	void* voidPointer = &garfield;
	// voidPointer->saySomething();
	// voidPointer->isPurring();
	

	// doesn't work in the other direction
	Animal unicorn("Unicorn");
	// Cat* catPointer2 = &unicorn;

	// or with classes we don't inherit
	Dog scoobyDoo("Scooby Doo");
	// Cat* catPointer3 = &scoobyDoo;
}