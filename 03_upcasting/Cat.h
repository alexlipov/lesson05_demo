﻿#pragma once
#include "Animal.h"
#include <string>

class Cat : public Animal
{
	bool _purring; // מגרגר
public:
	Cat(std::string name);

	void saySomething(); // method hiding

	void setPurring(bool value);
	bool isPurring();
};

