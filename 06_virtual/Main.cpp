#include "Animal.h"
#include "Cat.h"
#include "Dog.h"

// we want to cut the tie between
// variable type and invoked method;
void main() {
	Cat garfield("Garfield");
	Dog scoobyDoo("Scooby Doo");

	Animal* animals[2];
	animals[0] = &garfield;
	animals[1] = &scoobyDoo;

	for (int i = 0; i < 2; i++) {
		animals[i]->saySomething();
	}
}